package org.usfirst.frc.team1711.robot;


import edu.wpi.first.wpilibj.Servo;
import edu.wpi.first.wpilibj.Talon;
import edu.wpi.first.wpilibj.AnalogPotentiometer; 
import edu.wpi.first.wpilibj.AnalogGyro; 
import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.DigitalInput;
import edu.wpi.first.wpilibj.AnalogInput;

//button 6 & 7 shooter angle adjustment, button 3 enable shooting, button 5 disable shooting, button 1 fire the disable shooting. 


public class Shooter 
{
	// potentiometer outputs for shooter position
	double potHighest=0.300;
	double potMidPoint=0.633;
	double potLowest=0.685;
	
	Servo motorShooterKicker;
	Servo motorCameraAngle;
	Servo motorCameraTilt;
	Talon motorShooterPitchLeft;
	Talon motorShooterPitchRight;
	Talon motorShooterLeft;
	Talon motorShooterRight;
	AnalogGyro gyro=null;
	AnalogPotentiometer pot=null;
	DigitalInput intakeLimit=null;
	AnalogInput ultra=null;
	int lastJogButton=0;
	boolean atMax=false;
	boolean atMin=false;
	boolean inFireMode=false;
	boolean inCollectMode=false;
	Joystick shooterStick;
	
	public Shooter()
	{
		//Creating Servo objects
		if(RobotMap.motorShooterKicker != -1)
		{
			motorShooterKicker = new Servo(RobotMap.motorShooterKicker); 
		}
		
		if(RobotMap.motorCameraAngle != -1)
		{
			motorCameraAngle = new Servo(RobotMap.motorCameraAngle);
		}
		
		if(RobotMap.motorCameraTilt != -1)
		{
			motorCameraTilt = new Servo(RobotMap.motorCameraTilt);
		}
		
		//Creating Talon objects
		if(RobotMap.motorShooterPitchLeft != -1)
		{
			motorShooterPitchLeft = new Talon(RobotMap.motorShooterPitchLeft);
		}
		
		if(RobotMap.motorShooterPitchRight != -1)
		{
			motorShooterPitchRight = new Talon(RobotMap.motorShooterPitchRight);
		}
		
		if(RobotMap.motorShooterLeft != -1)
		{
			motorShooterLeft = new Talon(RobotMap.motorShooterLeft);
		}
		
		if(RobotMap.motorShooterRight != -1)
		{
			motorShooterRight = new Talon(RobotMap.motorShooterRight);
		}
		
		//creating joystick
		if(RobotMap.joystick1 != -1)
		{
			shooterStick = new Joystick(RobotMap.joystick1);
		}
		
		// define the shooter potentiometer
		if(RobotMap.shooterPot != -1)
		{
			pot=new AnalogPotentiometer(RobotMap.shooterPot);
		}
	}
	
	public void fireControl(Joystick shooterStick) 
	//controls shooting the ball
	{
		if(motorShooterKicker != null && motorShooterLeft != null && motorShooterRight != null && shooterStick != null)
		{

			// look for an enable/disable request
			if(shooterStick.getRawButton(RobotMap.shooterStickBtnShooterEnable))
				inFireMode=true;
			if(shooterStick.getRawButton(RobotMap.shooterStickBtnShooterDisable))
				inFireMode=false;
			
			// look for a shoot request
			if(shooterStick.getRawButton(RobotMap.shooterStickBtnShoot)) {
				// only shoot if the shooter is enabled
				if(inFireMode) {
					System.out.println("Fire Enabled");
					// perform the following on a thread
					// to allow the rest of the robot controls
					// to be enabled
					Thread thread = new Thread() {
					    public void run() {			    
					    	try {
						    	double initialSpeed=0.3;	// initial wheel speed
						    	double shootSpeed=0.8;		// shooting speed
						    	motorShooterLeft.set(-initialSpeed);
						    	motorShooterRight.set(-initialSpeed);
						    	sleep(750); 				// wait half second to get up to speed
						    	motorShooterLeft.set(-shootSpeed);
						    	motorShooterRight.set(-shootSpeed);
						    	sleep(750);					// start to spin-up
						    	motorShooterKicker.setAngle(0);	// SHOOT
						    	sleep(1000);					// wait for ball release	
								motorShooterKicker.setAngle(90);	// retract the kicker
		
								// shut down the motors and disable fire control
						    	motorShooterLeft.set(0);
						    	motorShooterRight.set(0);
								inFireMode=false;				
						    } catch(InterruptedException ie) {}
					    }
					};
				    thread.start();
				}
			}
		}
	}
	
	public void cameraCenter() 
	//centers the camera on the rotating mount in the x-y direction
	//called when the robot is initialized
	{
		if(motorCameraAngle != null)
		{
			motorCameraAngle.setAngle(80);
			System.out.println("set");
		}
	}
	
	public void cameraAngle (final Joystick shooterStick) 
	//controls the angle and tilt of the camera mount
	{
		if(shooterStick != null && motorCameraAngle != null)
		{
			double servoAngle = motorCameraAngle.getAngle();
			
			//angles the camera right
			if (shooterStick.getRawButton(RobotMap.cameraAngleRightButton)) {
				motorCameraAngle.setAngle(servoAngle - 1);		
			}
					
			//angles the camera left
			if (shooterStick.getRawButton(RobotMap.cameraAngleLeftButton)) {
				motorCameraAngle.setAngle(servoAngle + 1);
			}
			
			//centers the camera in the x-y direction
			if (shooterStick.getRawButton(RobotMap.cameraCenterViewButton)) {
				motorCameraAngle.setAngle(90);
			}
		}
		
		if(motorCameraTilt != null && pot != null)
		{
			//this is unfinished
			//throttle control on the shooter joystick should control the tilt of the camera
			//and it should move with the shooter
			double shooterTilt = pot.get();
			double cameraTilt = motorCameraTilt.get();
			
			//use this line to test the servo readings 
			System.out.println(motorCameraTilt.get());
		}
		
	} 

	public void collectorControl(Joystick shooterStick) 
	//controls the intake of the ball
	{
		if(shooterStick != null && motorShooterLeft != null && motorShooterRight != null && pot != null && motorShooterPitchLeft != null && motorShooterPitchRight != null)
		{
			if(shooterStick.getRawButton(RobotMap.shooterStickBtnShooterCollect)) {
		    	double collectorSpeed=0.5;	//  wheel speed

		    	// disable the fire mode
	    		inFireMode=false;
	    		
	    		// set the collect mode
	    		inCollectMode=true;
	    		motorShooterLeft.set(collectorSpeed);
	    		motorShooterRight.set(collectorSpeed);
	    		
	    		// move the shooter down to collect level
		    	if(pot.get()<potLowest) {
		    		motorShooterPitchLeft.set(-1);
		    		motorShooterPitchRight.set(-1);
		    	}
		    	else {
		    		motorShooterPitchLeft.set(0);
		    		motorShooterPitchRight.set(0);
		    	}
			}
			else {
				// if was in collect mode then switch off wheels and lift 
				// shooter to mid point. Thread the operation to not affect
				// machine performance
				if(inCollectMode) {
			    	motorShooterLeft.set(0);
			    	motorShooterRight.set(0);
			    	Thread thread = new Thread() {
					    public void run() {			    
					    	// move the pitch to mid level
					    	while(pot.get()>potMidPoint) {
					    		motorShooterPitchLeft.set(1);
					    		motorShooterPitchRight.set(1);
					    	}
					    	motorShooterPitchLeft.set(0);
					    	motorShooterPitchRight.set(0);
					    }
					};
				    thread.start();
				    inCollectMode=false;
				}		
			}
		}
		
	}
	public void liftPitch ()
	//this method raises the pitch to the max value
	//did not use the != null here, because if the shooter comes off, we won't use it in auton obviously
	{
		Thread thread = new Thread() {
			public void run() {
				{
					while (pot.get()>potHighest) {			// tested value for topmost
						motorShooterPitchLeft.set(1);
						motorShooterPitchRight.set(1);// upper limit hit, stop move
						
					}
				}
				atMax=true;
				motorShooterPitchLeft.set(0);
				motorShooterPitchRight.set(0);
				
			}
		};
		thread.start();
	}
	
	public void lowerPitch ()
	//this method lowers the pitch to the min value
	//did not use the != null here, because if the shooter comes off, we won't use it in auton obviously
	{
		Thread thread = new Thread() 
		{
			public void run() 
			{					
				while(pot.get()<.640) // tested value for bottommost
				{				
					motorShooterPitchLeft.set(-1);
					motorShooterPitchRight.set(-1);
					
				}
				// lower limit hit, stop move
				motorShooterPitchLeft.set(0);					
				motorShooterPitchRight.set(0); 
				atMin=true;
			}
		};
		thread.start();
	
	}
	
	public void partialLift(double potValue)
	//this method raises the pitch part of the way
	{ 
		Thread thread = new Thread() 
		{
			public void run() 
			{
				//if the shooter is too low, raise it
				while(pot.get() > potValue) 
				{
					motorShooterPitchLeft.set(-1);
					motorShooterPitchRight.set(-1);
				}
				//when the pot value is reached, stop
				motorShooterPitchLeft.set(0);
				motorShooterPitchLeft.set(0);
			}
		};
		thread.start();
	}
	
	public void partialLower(double potValue)
	//this method lowers the pitch part of the way
	{
		//if the shooter is too high, raise it
		while(pot.get() < potValue)
		{
			motorShooterPitchLeft.set(1);
			motorShooterPitchRight.set(1);
		}
		//when the pot value is reached, stop
		motorShooterPitchLeft.set(0);
		motorShooterPitchLeft.set(0);
	}

	public void pitchControl(Joystick shooterStick) 
	//this controls the raising and lowering of the shooter
	{		
		//NEVER TAKE THIS OUT EVER
		//NEVER
		//System.out.println(pot.get());
		
		// use the potentiometer to measure angle of shooter (if it exists)
	//	System.out.println(pot.get());
		if(pot!=null && motorShooterPitchLeft!=null && motorShooterPitchRight!=null) 
		{
			if(pot.get()<potHighest) {			// tested value for topmost
				motorShooterPitchLeft.set(0);
				motorShooterPitchRight.set(0);// upper limit hit, stop move
				atMax=true;
				atMin = false;
				System.out.println("At max");
				
			}
			if(pot.get()>potLowest) {			// tested value for bottommost
				motorShooterPitchLeft.set(0);
				motorShooterPitchRight.set(0); // lower limit hit, stop move
				atMin=true;
				atMax = false;
				System.out.println("At min");
			}		
		}
		if(shooterStick!=null && motorShooterPitchLeft!=null && motorShooterPitchRight!=null)
		{
			// detect jog up if not at max
			if(!atMax) 
			{
				if(shooterStick.getRawButton(RobotMap.shooterStickBtnJogUp)) {
					motorShooterPitchLeft.set(1);
					motorShooterPitchRight.set(1);
					atMin=false;
					lastJogButton=RobotMap.shooterStickBtnJogUp;
				}
				
				else if(lastJogButton==RobotMap.shooterStickBtnJogUp) {
					motorShooterPitchLeft.set(0);
					motorShooterPitchRight.set(0);
					lastJogButton=0;
				}
			}
			else
				lastJogButton=0;
			
			// detect jog down if not at min
			if(!atMin) {
				if(shooterStick.getRawButton(RobotMap.shooterStickBtnJogDown)) {
					motorShooterPitchLeft.set(-1);
					motorShooterPitchRight.set(-1);
					atMax=false;
					lastJogButton=RobotMap.shooterStickBtnJogDown;
				}
				else if(lastJogButton==RobotMap.shooterStickBtnJogDown) {
					motorShooterPitchLeft.set(0);
					motorShooterPitchRight.set(0);
					lastJogButton=0;
				}
			}
			else
				lastJogButton=0;
		}
		
	}
}
